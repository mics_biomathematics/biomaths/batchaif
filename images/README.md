Images
======

## Description
Images for the AIF model architecture's description.

## Repository structure
- **`AIF_architecture.png`**: image of the AIF architecture.
- **`aif_losses.png`**: image of the AIF losses.
